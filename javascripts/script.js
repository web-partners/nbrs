(function($){
	$('.back-to-top').on('click', function(e){
		e.preventDefault();
		$('html, body').animate({ scrollTop: 0 }, 400);
	});
	$('#tabs').tabulous({
    	effect: 'scale'
    });
})(jQuery);